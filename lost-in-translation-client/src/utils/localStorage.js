export const setStorage = (key, value) => {
    // Stringify data
    const json = JSON.stringify(value)
    // Encrypt
    const encrypted = btoa( json )
    // Save in localStorage
    localStorage.setItem(key, encrypted)
}

export const getStorage = (key) => {
    // Get from localStorage
    const storedValue = localStorage.getItem(key)
    if(!storedValue){
        return false
    }
    // Decrypt
    const decrypted = atob(storedValue)
    // Parse and return JSON
    return JSON.parse(decrypted)
}