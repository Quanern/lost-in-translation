import loginStatusReducer from "./loginStatus";
import {combineReducers} from "redux";

// Combines the reducers, even though there is only one now, it is here in case this project is expanded upon.
const rootReducers = combineReducers({
    loginStatus: loginStatusReducer,
});

export default rootReducers