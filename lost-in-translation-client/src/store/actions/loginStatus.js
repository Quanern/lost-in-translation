//Type
export const SET_LOGIN_STATUS = 'SET_LOGIN_STATUS'

//Actions
export const setLoginStatusAction = (loginStatus = false) => ({
    type: SET_LOGIN_STATUS,
    loginStatus,
});
