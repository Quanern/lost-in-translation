import {createStore} from "redux";
import rootReducers from "./reducers";

//Creates the store
const store = createStore(
    rootReducers,
    //Dev-tool extension for redux.
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store