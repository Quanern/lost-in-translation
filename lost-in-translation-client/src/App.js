import './App.css';
import "./utils/colors.css"
import Navbar from "./components/Navbar/Navbar";
import Login from "./components/Login/Login";
import Translation from "./components/Translation/Translation";
import Profile from "./components/Profile/Profile";
import NotFound from "./components/NotFound/NotFound";
import {BrowserRouter as Router, Switch} from "react-router-dom";
import PrivateRoute from "./components/hoc/PrivateRoute";
import PublicRoute from "./components/hoc/PublicRoute";

//The application's routes and the routes' components.
function App() {
  return (
    <div className="App backgroundColor">
        <Router>
            <Navbar/>
            <Switch>
                <PrivateRoute path="/translation" component={Translation}/>
                <PrivateRoute path="/profile" component={Profile}/>
                <PublicRoute exact path="/"  component={Login}/>
                <PublicRoute path="*" component={NotFound}/>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
