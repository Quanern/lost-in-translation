import "./Profile.css"
import "../../utils/colors.css"
import {useHistory} from "react-router-dom";
import {useState} from "react";
import {useDispatch} from "react-redux";
import {setLoginStatusAction} from "../../store/actions/loginStatus";
import {getStorage} from "../../utils/localStorage";
import Card from "../hoc/Card";

const Profile = () => {
    // Hooks for pushing the user to another page and to set the global state respectively.
    const history = useHistory()
    const dispatch = useDispatch()

    // Sets the translate history to what is saved in the localStorage or an empty array.
    const [translateHistory, setTranslateHistory] =
        useState(JSON.parse(localStorage.getItem('translate_history')) || [])

    // Gets the user's username from the localStorage
    const {username} = getStorage('lit_session')

    // List items of the 10 last translations, with the newest one at the top
    const translateHistoryList =
        translateHistory.reverse().map((search, i) => {
            return (
                <li className="searchItems" key={i}>{search}</li>
            );
        })

    // When logout is clicked, the localStorage is cleared and the global state loginStatus is set to false,
    // meaning the user is logged out. Then it pushes the user to the login page.
    const logoutClicked = () =>{
        localStorage.clear()
        dispatch(setLoginStatusAction(false))
        history.push("/")
    }

    // When clear history is clicked, the saved history will be deleted.
    const clearClicked = () => {
        localStorage.removeItem('translate_history')
        setTranslateHistory([])
    }
    
    return(
        <Card>
            <h1 className="profileTitle">{username}</h1>
            { translateHistory.length>0 &&
            <section>
                <h2 className="searchTitle">Latest searches</h2>
                <ol id="listOfSearches" className="secondaryColor">
                    {translateHistoryList}
                </ol>
            </section>}

            <section id="buttonSection">
                    <div>
                        <button id="logoutButton" className="profileButtons secondaryColor" onClick={logoutClicked}>Logout!</button>
                        <button id="clearButton" className="profileButtons secondaryColor" onClick={clearClicked}>Clear search!</button>
                    </div>
            </section>
        </Card>
    )
}

export default Profile