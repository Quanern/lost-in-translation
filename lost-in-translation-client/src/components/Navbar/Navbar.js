import logo from "../../utils/images/Logo.png"
import loggedInLogo from "../../utils/images/Logo-Hello.png"
import "./Navbar.css"
import "../../utils/colors.css"
import { NavLink} from "react-router-dom";
import {useSelector} from "react-redux";
import {getStorage} from "../../utils/localStorage";

const Navbar = () => {
    //Gets the state from the store and the username from the localStorage.
    const isLoggedIn = useSelector(state => state.loginStatus)
    const {username} = getStorage('lit_session')

    // Renders different depending on whether the user is logged in or not.
    return(
        <nav id="outerNavbarDiv" className="primaryColor">
            {!isLoggedIn && <div id="innerNavbarDiv">
                <img id="homeLogoNotLoggedIn" src={logo} alt="logo"/>
                <span className="navbarText">Lost in Translation</span>
            </div>}
            {isLoggedIn && <div id="innerNavbarDiv">
                <NavLink className="navLinks" id="navLinkTranslate" to="/translation" activeClassName="selected">
                    <img id="homeLogo" src={loggedInLogo} alt="logo"/>
               Lost in Translation
                </NavLink>
                <NavLink className="navLinks" id="navLinkProfile" to="/profile" activeClassName="selected">
                    {username} <span className="material-icons profileLogo">account_box</span>
                </NavLink>
            </div>}
        </nav>
    )
}

export default Navbar