const API_USER_URL = 'http://localhost:3000/users'

const handleResponse = response => {
    if(response.status >= 400){
        throw Error(response.error)
    }
    return response
}

export const registerUser = (username, id) => {
    return fetch(
        API_USER_URL, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id,
                username: username
            })
        })
        .then(r => r.json())
        .then(handleResponse)
}

export const loginUser = () => {
    return fetch(API_USER_URL)
        .then(result => {return result.json()})
        .then(handleResponse)
}