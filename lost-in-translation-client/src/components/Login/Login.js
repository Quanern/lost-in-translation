import { Redirect } from 'react-router-dom';
import LoginForm from "./LoginForm";
import {useHistory} from "react-router-dom";
import {getStorage, setStorage} from "../../utils/localStorage";
import {setLoginStatusAction} from "../../store/actions/loginStatus";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import Card from "../hoc/Card";

const Login = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const isLoggedIn = useSelector(state => state.loginStatus)

    // Life-cycle hook
    useEffect(() => {
        // Gets from the localStorage
        const loggedIn = getStorage('lit_session')
        // Sets the loginStatus as false if the lit_session doesn't exist in localStorage,
        // meaning the user isn't logged in.
        dispatch(setLoginStatusAction(Boolean(loggedIn)))
    } )

    // When the log-in button is clicked, if the user refused to be added to the database, then nothing will happen
    const handleLoginClicked = (result) => {
        if(!result){
            return
        }
        //Destructure the json-object result into username and id, and set them in the localStorage
        const {username, id} = result
        //Checks (for the 2nd time) that the username exists.
        if(username){
            // Sets the username and id
            setStorage('lit_session', {
                username,
                id
            })
            // Changes the global state of user to true, meaning that we are logged in.
            dispatch(setLoginStatusAction(true))
            // Pushes user to the translation page.
            history.replace("/translation")
        }
    }
    // If the user is already logged in, they will be redirected to the translation page
    return(
        <Card>
            { isLoggedIn && <Redirect to="/translation"/>}
            <LoginForm loginClicked={handleLoginClicked}/>
        </Card>
    )
}

export default Login