import {useState} from "react";
import {loginUser, registerUser} from "../api/userAPI";
import "./LoginForm.css"
import "../../utils/colors.css"


const LoginForm = props =>{
    // States and setters for the username, loading and error when logging in.
    const [username, setUsername] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [loginError, setLoginError] = useState('')

    // When the login button is clicked, the states are set to isLoading and clears the login error.
    const onLoginClicked = async(e) => {
        e.preventDefault()
        if(!username){
            return
        }
        setIsLoading(true)
        setLoginError('')
        let result
        try{
            // await the loginUser from the api that returns an array of users.
            const users = await loginUser()
            // filters away all the usernames that isn't the state username.
            result = users.find(user => username.toLowerCase() === user.username.toLowerCase())
            // if the result is empty, that there is none in the database with that name, a prompt will show
            // asking if the user wants to add the username to the database.
            if(!result){
                let confirm = window.confirm("User is not in the database, do you want to be added?")
                //If the user accepts, the username will then be added to the database.
                if(confirm){
                    result = await registerUser(username)
                }
            }
        }
        // Sets the error message if there is one.
        catch(error) {
            setLoginError(error.message || error)
        }
        // When all is done, whether successfully or not, loading will be set to false, then it sends the props to the
        // parent component.
        finally {
            setIsLoading(false)
            props.loginClicked(result)
        }

    }

    // Sets the state username to the value of the input box without spaces at the end and the beginning.
    const onUsernameChanged = event => {
        setUsername(event.target.value.trim())
    }

    return(
            <>
                <form onSubmit={onLoginClicked}>
                    <input id="inputUsername" type="text" placeholder="Enter your username" required="required"
                           onChange={ onUsernameChanged}/>
                    <input id="loginButton" className="secondaryColor" type="submit" value="Login"/>
                </form>
                { isLoading && <p>Logging in....</p>}
                { loginError && <p>{loginError}</p>}
            </>
    )
}

export default LoginForm