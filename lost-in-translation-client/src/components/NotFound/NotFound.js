import {Link} from "react-router-dom";
import "./NotFound.css"
import Card from "../hoc/Card";


function NotFound() {
    // A page not found page with a link to send the user to the login screen
    // which will redirect the user to the translation screen if they're already logged in
    return(
        <Card>
                <h1>Page Not Found!</h1>
                <Link to="/" exact>
                    Go back home
                </Link>
        </Card>
    )
}

export default NotFound