const TranslationDisplay = props => {
    //If the character is a whitespace, return an empty div as a block (pushes the next image to come beneath it)
    if (props.letter === ' ') {
        return <div style={{display: "block"}}/>;
    }

    // The path of the image for each letter
    let image = require('../../utils/images/alphabet/' + props.letter + '.png')
   // Returns the image of the letter.
    return (
        <>
            <img
                id='sign'
                src={image.default}
                alt={props.letter}
            />
        </>
    );
};

export default TranslationDisplay;
