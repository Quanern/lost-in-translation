import "./TranslationForm.css"
import "../../utils/colors.css"
import {useState} from "react";

const TranslationForm = props => {
    //State for the word(s) to be translated
    const [ translation, setTranslation ] = useState('')

    //List of the latest searches the user has done, empty if there is none
    let translateHistory = JSON.parse(localStorage.getItem('translate_history')) || []

    //Saves the updated list in the localStorage.
    const translationHistory = () => { localStorage.setItem('translate_history', JSON.stringify(translateHistory))}

    // What happens when the translate button is clicked
    const translationClicked = event =>{
        event.preventDefault()
        // If there is nothing to translate, nothing will happen.
        if(!translation){
            return
        }
        //If the words to be translated includes anything but letters, an alert will pop up.
        if(translation.match(/[^A-Z\s]/gi)){
            alert("Your search contains a number or a sign. Only letters and spaces are allowed")
            return
        }
        // Pushes the word to the array and if there are more than 11 translations in the array, remove the first one.
        translateHistory.push(translation)
        if(translateHistory.length > 10){
            translateHistory.shift()
        }
        // Saves the updated list in the localStorage and sends the translation to the parent component
        translationHistory()
        props.translationClicked({"translation": translation})
    }

    // Whenever a character is removed/entered, the translation will be updated to the new word(s)
    // without spaces at the end and the beginning. Sends a prop to the parent component that it's been triggered
    const onTranslationInputChange = event => {
        setTranslation(event.target.value.trim())
        props.inputChange()
    }

    // The input has a max length of 40 characters, spaces included.
    return(
        <form onSubmit={translationClicked}>
            <input type="text" id="translationInput"
                   placeholder="Enter your word" onChange={onTranslationInputChange}
                   maxLength={40}
            />
            <input type="submit" id="translationButton" className="secondaryColor" value="Translate!"/>
        </form>

    )
}

export default  TranslationForm