import React, {useState} from "react";
import "./Translation.css"
import TranslationDisplay from "./TranslationDisplay";
import TranslationForm from "./TranslationForm";
import Card from "../hoc/Card";

const Translation = () => {
    // States for translation, which are the words to be translated, and translate, whether the page should show
    // the translation or not.
    const [ translation, setTranslation ] = useState('')
    const [ translate, setTranslate ] = useState(false)

    // Whenever the translation button is clicked, the translate is set to true, showing the translation in images.
    // The translation is
    const handleTranslationClicked = ({translation}) => {
        setTranslate(true)
        setTranslation(translation)
    }

    // Whenever a character is removed/added to the input box, the state of translate will be set to false, hiding the
    // the translation in images.
    const handleInputChange = () => {
        setTranslate(false)
    }

    // Splits the words into single characters and returns the images (translation)
    const signList =
        translation.toLowerCase().split('').map((letter, i) => {
        return (
            <TranslationDisplay letter={letter} key={i}/>
        );
    });

    return(
        <Card>
            <section id="inputSection">
                <TranslationForm translationClicked={handleTranslationClicked} inputChange={handleInputChange}/>
            </section>
            <section id="translationSection">
                { translate && signList}
            </section>
        </Card>
    )
}


export default Translation