import "./Card.css"
import "../../utils/colors.css"
const Card = ({children}) => {
    return (
        <div className="card primaryColor">
            <div className="innerCard">
                {children}
            </div>
        </div>
    )
}

export default Card