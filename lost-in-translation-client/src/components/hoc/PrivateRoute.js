import { Redirect, Route } from "react-router-dom";
import {useSelector} from "react-redux";

const PrivateRoute = props => {

    const isLoggedIn = useSelector(state => state.loginStatus)

    if (!isLoggedIn) {
        return <Redirect exact to="/" />
    }
    return <Route {...props} />
}
export default PrivateRoute