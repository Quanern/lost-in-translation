# Lost in Translation
This React project is a webpage that translates letters into their sign language equivalent.
## Running the project
The project has a server and a client. to run them, you need to open two terminals, one in the server and one in the 
client folder. Then you run:
```
npm install
```

When the project is done installing the necessary modules, run:
```
npm start
```
Do this in both terminals. The server will run on port 3000, while the client will run on port 3006

## Webpage
The webpage will start on the login screen. After logging in you can navigate between the different views with the 
navbar on top.
### Login
To be able to use the website, you have to first log in with a username. If your username is not in the database, you
will be asked if you want to be added to the database or not. If you refuse, you will not be logged in, and if you 
accept, you will be added to the database and can use the same username the next time you log in.

### Translation
After logging in, you will be redirected to the translation page. Here you can type in what words (letters) you want 
translated into sign language. After you click the translate-button, if there is any invalid characters in the input, 
an alert will show and tell you that it only takes letters and spaces as valid characters. 

If the string is valid, a box with images of all the signs will show up beneath the input. If it is a long word, images
will continue beneath if it gets too wide. If there is a space, then the next word will start beneath the previous word.

By typing or changing what's in the input box, the images will be hidden and won't show until you hit the 
translate-button again. Your 10 most recent searches will be added to your browser's local storage, and can be seen
on your profile page.

### Profile
In your profile you will find a list of your last searches, with the newest on top, and up to a maximum of 10 searches.
You can also clear your search history and logout of the webpage here.

## Server
This project uses JSON-server as it's server. More information can be found by reading the server's README.md
